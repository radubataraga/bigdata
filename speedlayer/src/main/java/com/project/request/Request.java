package com.project.request;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


public class Request {

    private String productionDate;
    private String toolId;
    private String sensorId;
    @JsonInclude(Include.NON_NULL)
    private Integer pressure;
    @JsonInclude(Include.NON_NULL)
    private Double temp;
    @JsonInclude(Include.NON_NULL)
    private Integer humidity;


    public Request(String productionDate, String toolId, String sensorId, Integer pressure, Double temp, Integer humidity) {
        this.productionDate = productionDate;
        this.toolId = toolId;
        this.sensorId = sensorId;
        this.pressure = pressure;
        this.temp = temp;
        this.humidity = humidity;
    }


    public String getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(String productionDate) {
        this.productionDate = productionDate;
    }

    public Request() {
    }

    public String getToolId() {
        return toolId;
    }

    public void setToolId(String toolId) {
        this.toolId = toolId;
    }

    public String getSensorId() {
        return sensorId;
    }

    public void setSensorId(String sensorId) {
        this.sensorId = sensorId;
    }

    public Integer getPressure() {
        return pressure;
    }

    public void setPressure(Integer pressure) {
        this.pressure = pressure;
    }

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }
}
