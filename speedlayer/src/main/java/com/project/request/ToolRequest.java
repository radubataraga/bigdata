package com.project.request;


import com.fasterxml.jackson.annotation.JsonInclude;

public class ToolRequest {
    private Integer partId;
    private Integer batchId;
    private String productionDate;
    private String toolId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double  temperature;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double  humidity;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double  pressure;

    public ToolRequest() {
    }

    public ToolRequest(Integer partId, Integer batchId, String productionDate, String toolId, Double temperature, Double humidity, Double pressure) {
        this.partId = partId;
        this.batchId = batchId;
        this.productionDate = productionDate;
        this.toolId = toolId;
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
    }

    public Integer getPartId() {
        return partId;
    }

    public void setPartId(Integer partId) {
        this.partId = partId;
    }

    public Integer getBatchId() {
        return batchId;
    }

    public void setBatchId(Integer batchId) {
        this.batchId = batchId;
    }

    public String getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(String productionDate) {
        this.productionDate = productionDate;
    }

    public String getToolId() {
        return toolId;
    }

    public void setToolId(String toolId) {
        this.toolId = toolId;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }
}
