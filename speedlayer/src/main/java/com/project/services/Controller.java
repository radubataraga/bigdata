package com.project.services;

import com.project.request.Request;
import org.elasticsearch.rest.RestStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {


    @Autowired
    private Streamer streamer;



    @PostMapping("/data")
    public RestStatus insertData(@RequestBody Request req) {

        System.out.println(req.toString());
        streamer.insert(req);
        return RestStatus.ACCEPTED;
    }

}
