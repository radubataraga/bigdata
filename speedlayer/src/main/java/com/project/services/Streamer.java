package com.project.services;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.request.Request;
import com.project.request.ToolRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.rest.RestStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.PrivateKey;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Map;

@Service
public class Streamer {

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private RestHighLevelClient restHighLevelClient;


    public RestStatus insert(Request message) {

        IndexRequest indexRequest = new IndexRequest();
//        TypeReference<HashMap<String, Object>> ref = new TypeReference<HashMap<String, Object>>() {};
        try {
        Map<String, Object> map;
            LocalDateTime  localDateTime = LocalDateTime.of(LocalDate.now().getYear(), Month.JANUARY, LocalDate.now().getDayOfMonth(), LocalDateTime.now().getHour(),
                    LocalDateTime.now().getMinute(), LocalDateTime.now().getSecond());

            String date = localDateTime.toString();
            message.setProductionDate(date);

        map = mapper.convertValue(message, Map.class);
        indexRequest.index("bosch_speed_sensor");
        indexRequest.source(map, XContentType.JSON);
        IndexResponse response = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);


            ToolRequest request = new ToolRequest();
            request.setToolId(message.getToolId());
            request.setProductionDate(date);

            if(message.getToolId().equalsIgnoreCase("1")) {
                request.setPressure(Double.valueOf(message.getPressure()));
            }

            if(message.getToolId().equalsIgnoreCase("2")) {
                request.setTemperature(message.getTemp());
            }

            if(message.getToolId().equalsIgnoreCase("3")) {
                request.setHumidity(Double.valueOf(message.getHumidity()));
            }

            map = mapper.convertValue(request, Map.class);
            indexRequest.index("bosch_speed_tool");
            indexRequest.source(map, XContentType.JSON);
            IndexResponse response2 = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);



            return response.status();
        } catch (IOException e) {
            System.out.println("ERROR"+e.getMessage());
            return RestStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            System.out.println("ERROR"+e.getMessage());
            return RestStatus.BAD_REQUEST;
        }
    }
}
