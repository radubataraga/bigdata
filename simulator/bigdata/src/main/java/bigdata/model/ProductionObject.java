package bigdata.model;


//@Entity
//@Table(name = "Production5")
public class ProductionObject {

//	@Id
//	@Column(name = "partId")
	private Integer partId ;

//	@Column(name = "batchId")
	private Integer batchId ;

//	@Column(name = "productionTimestamp")
//	private LocalDate productionTimestamp;

//	@Column(name = "productionDate")
	private String productionDate;

//	@Column(name = "toolId")
	private int  toolId ;

//	@Column(name = "temperature")
	private Double temperature;

//	@Column(name = "pressure")
	private Double pressure;

//	@Column(name = "humidity")
	private Double humidity;

//	@Column(name = "upperLimit")
	private String upperLimit;

//	@Column(name = "lowerLimit")
	private String lowerLimit;

	public ProductionObject() {
	}

	public Double getHumidity() {
		return humidity;
	}

	public void setHumidity(Double humidity) {
		this.humidity = humidity;
	}


	public Integer getPartId() {
		return partId;
	}

	public void setPartId(Integer partId) {
		this.partId = partId;
	}

	public Integer getBatchId() {
		return batchId;
	}

	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}

	public String getProductionDate() {
		return productionDate;
	}

	public void setProductionDate(String productionDate) {
		this.productionDate = productionDate;
	}

	public int getToolId() {
		return toolId;
	}

	public void setToolId(int toolId) {
		this.toolId = toolId;
	}

	public Double getTemperature() {
		return temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	public Double getPressure() {
		return pressure;
	}

	public void setPressure(Double pressure) {
		this.pressure = pressure;
	}

	public String getUpperLimit() {
		return upperLimit;
	}

	public void setUpperLimit(String upperLimit) {
		this.upperLimit = upperLimit;
	}

	public String getLowerLimit() {
		return lowerLimit;
	}

	public void setLowerLimit(String lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	@Override
	public String toString() {
		return "ProductionObject{" + "partId=" + partId + ", batchId=" + batchId + ", productionDate='" + productionDate + '\'' + ", toolId=" + toolId
				+ ", temperature=" + temperature + ", pressure=" + pressure + ", humidity=" + humidity + ", upperLimit='" + upperLimit + '\''
				+ ", lowerLimit='" + lowerLimit + '\'' + '}';
	}
}
