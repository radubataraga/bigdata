package bigdata.service;

import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

@Service
public class TimeService {


    public String extractDate(Integer dayDate) {
        LocalDateTime localDateTime;
        if (dayDate != null) {
            localDateTime =LocalDateTime.of(LocalDate.now().getYear(), Month.JANUARY,dayDate, LocalDateTime.now().getHour(),
                    LocalDateTime.now().getMinute(), LocalDateTime.now().getSecond() );
        } else {
            localDateTime =LocalDateTime.of(LocalDate.now().getYear(), Month.JANUARY, LocalDate.now().getDayOfMonth(), LocalDateTime.now().getHour(),
                    LocalDateTime.now().getMinute(), LocalDateTime.now().getSecond());
        }
        return localDateTime.toString();
    }
}
