package bigdata.controller;

import bigdata.model.ProductionObject;
import bigdata.service.TimeService;
import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@RestController
public class Controller {

//	@Autowired
//	private ProductionObjectRepository productionObjectRepository;

	@Autowired
	private TimeService timeService;

	private int partIdMultiplier = 1000000000;
	private int batchIdMultiplier = 1000000000;

	@RequestMapping(value = "/big/{dayDate}", method = RequestMethod.GET)
	public ResponseEntity<String> retrieveAllPartnersSettingsWithoutParams(@PathVariable Integer dayDate) throws IOException {

		int noOfRepetitions = 400;
		int batchCount = 1;
		Date date = new Date();
//		date.toInstant().atZone(ZoneId.systemDefault()).


		Random random = new Random();
		List<ProductionObject> productionObjectList = new ArrayList<>();
		for (int count = 1; count < noOfRepetitions; count++) {
			ProductionObject productionObject = new ProductionObject();
			if (count % 200 == 0) {
				batchCount += 1;
			}
			productionObject.setBatchId(batchCount + batchIdMultiplier);
			productionObject.setPartId(count + partIdMultiplier);
			//mean 15 and 60 deviation
//			nextGaussian()*desiredStandardDeviation+desiredMean;
//			productionObject.setProductionTimestamp(LocalDate.of(LocalDate.now().getYear(), Month.JANUARY, LocalDate.now().getDayOfMonth()));
			productionObject.setProductionDate(timeService.extractDate(dayDate));
			if (count % 3 == 0) {
				productionObject.setToolId(1);
				productionObject.setPressure(random.nextGaussian() * 300 + 700);
			} else if (count % 3 == 1) {
				productionObject.setToolId(2);
				productionObject.setTemperature(random.nextGaussian() * 40 + 20);
			} else{
				productionObject.setToolId(3);
				productionObject.setHumidity(random.nextGaussian() * 45+ 55);
			}
			Gson gson = new Gson();
			productionObjectList.add(productionObject);
			File file = new File("filename.txt");
			FileUtils.writeStringToFile(
					file, gson.toJson(productionObject) + "\n", StandardCharsets.UTF_8, true);
//			try (PrintWriter out = new PrintWriter("filename.txt")) {
//				out.println(productionObject);
//			} catch (FileNotFoundException e) {
//				e.printStackTrace();
//			}
		}
//		productionObjectRepository.saveAll(productionObjectList);
		return ResponseEntity.ok("");
	}



	public static void main(String[] args) {
		LocalDate localDate = LocalDate.now();
		System.out.println(localDate);
		LocalDate localDate2 = LocalDate.of(LocalDate.now().getYear(), Month.JANUARY, LocalDate.now().getDayOfMonth());
		LocalDateTime localDateTime =LocalDateTime.of(LocalDate.now().getYear(), Month.JANUARY, LocalDate.now().getDayOfMonth(), LocalDateTime.now().getHour(), LocalDateTime.now().getMinute(), LocalDateTime.now().getSecond());
		System.out.println(localDate2);
		Gson gson = new Gson();
		String jsonDate = gson.toJson(localDateTime);
		System.out.println(jsonDate);

	}
}
