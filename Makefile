DOCKER_NETWORK = bigdata_default
ENV_FILE = hadoop.env
current_branch := 2.0.0-hadoop3.1.3-java8
build:
	sudo docker build -t bde2020/hadoop-base:$(current_branch) ./base
	sudo docker build -t bde2020/hadoop-namenode:$(current_branch) ./namenode
	sudo docker build -t bde2020/hadoop-datanode:$(current_branch) ./datanode
	sudo docker build -t bde2020/hadoop-resourcemanager:$(current_branch) ./resourcemanager
	sudo docker build -t bde2020/hadoop-nodemanager:$(current_branch) ./nodemanager
	sudo docker build -t bde2020/hadoop-historyserver:$(current_branch) ./historyserver
	sudo docker build -t bde2020/hadoop-submit:$(current_branch) ./submit

wordcount:
	sudo docker build -t hadoop-wordcount ./submit
	sudo docker run --network ${DOCKER_NETWORK} --env-file ${ENV_FILE} bde2020/hadoop-base:$(current_branch) hdfs dfs -mkdir -p /input/
	sudo docker run --network ${DOCKER_NETWORK} --env-file ${ENV_FILE} bde2020/hadoop-base:$(current_branch) hdfs dfs -copyFromLocal /opt/hadoop-3.1.3/README.txt /input/
	sudo docker run --network ${DOCKER_NETWORK} --env-file ${ENV_FILE} hadoop-wordcount
	sudo docker run --network ${DOCKER_NETWORK} --env-file ${ENV_FILE} bde2020/hadoop-base:$(current_branch) hdfs dfs -cat /output/*
	sudo docker run --network ${DOCKER_NETWORK} --env-file ${ENV_FILE} bde2020/hadoop-base:$(current_branch) hdfs dfs -rm -r /output
	sudo docker run --network ${DOCKER_NETWORK} --env-file ${ENV_FILE} bde2020/hadoop-base:$(current_branch) hdfs dfs -rm -r /input
