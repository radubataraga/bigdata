To run the application we need first to create the images for hadoop.

IN LINUX BASED OS:
 just write ```make build``` and the images will be created.

IN WINDOWS:

 docker build -t bde2020/hadoop-base:2.0.0-hadoop3.1.3-java8 ./base
 docker build -t bde2020/hadoop-namenode:2.0.0-hadoop3.1.3-java8 ./namenode
 docker build -t bde2020/hadoop-datanode:2.0.0-hadoop3.1.3-java8 ./datanode
 docker build -t bde2020/hadoop-resourcemanager:2.0.0-hadoop3.1.3-java8 ./resourcemanager
 docker build -t bde2020/hadoop-nodemanager:2.0.0-hadoop3.1.3-java8 ./nodemanager
 docker build -t bde2020/hadoop-historyserver:2.0.0-hadoop3.1.3-java8 ./historyserver
 docker build -t bde2020/hadoop-submit:2.0.0-hadoop3.1.3-java8 ./submit



After this step phase just run docker-compose up -d and the system will work as inteded.

To test that hadoop works:

IN LINUX:
  run in terminal ```make wordcount```

IN WINDOWS:

  docker build -t hadoop-wordcount ./submit
  docker run --network bigdata_default --env-file hadoop.env bde2020/hadoop-base:2.0.0-hadoop3.1.3-java8 hdfs dfs -mkdir -p /input/
  docker run --network bigdata_default --env-file hadoop.env bde2020/hadoop-base:2.0.0-hadoop3.1.3-java8 hdfs dfs -copyFromLocal /opt/hadoop-3.1.3/README.txt /input/
  docker run --network bigdata_default --env-file hadoop.env hadoop-wordcount
  docker run --network bigdata_default --env-file hadoop.env bde2020/hadoop-base:2.0.0-hadoop3.1.3-java8 hdfs dfs -cat /output/*
  docker run --network bigdata_default --env-file hadoop.env bde2020/hadoop-base:2.0.0-hadoop3.1.3-java8 hdfs dfs -rm -r /output
  docker run --network bigdata_default --env-file hadoop.env bde2020/hadoop-base:2.0.0-hadoop3.1.3-java8 hdfs dfs -rm -r /input
