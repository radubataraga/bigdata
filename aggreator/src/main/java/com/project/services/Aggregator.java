package com.project.services;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.dtos.Request;
import com.project.dtos.ToolRequest;
import org.apache.logging.log4j.util.IndexedReadOnlyStringMap;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.rest.RestStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component
public class Aggregator {

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @Autowired
    private ObjectMapper mapper;

    public void createRequest(ToolRequest toolRequest) {
        IndexRequest indexRequest = new IndexRequest();
        try {
            Map<String, Object> map;

//            ToolRequest toolRequest = mapper.readValue(message, ToolRequest.class);

            map = mapper.convertValue(toolRequest, Map.class);
            indexRequest.index("bosch_batch_tool");
            indexRequest.source(map, XContentType.JSON);
            IndexResponse response = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
//            System.out.println(inde);
        } catch (IOException e) {
            System.out.println("ERROR"+e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println("ERROR"+e.getMessage());

        }
    }

    public void createRequestSensor(ToolRequest toolRequest) {
        IndexRequest indexRequest = new IndexRequest();
        try {
            Map<String, Object> map;
//            ToolRequest toolRequest = mapper.readValue(message, ToolRequest.class);

            map = mapper.convertValue(toolRequest, Map.class);
            indexRequest.index("bosch_batch_tool");
            indexRequest.source(map, XContentType.JSON);
            IndexResponse response = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            System.out.println("ERROR"+e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println("ERROR"+e.getMessage());

        }
    }



    public void createRequestSensor(Request toolRequest) {
        IndexRequest indexRequest = new IndexRequest();
        try {
            Map<String, Object> map;
//            ToolRequest toolRequest = mapper.readValue(message, ToolRequest.class);

            map = mapper.convertValue(toolRequest, Map.class);
            indexRequest.index("bosch_batch_sensor");
            indexRequest.source(map, XContentType.JSON);
            IndexResponse response = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            System.out.println("ERROR"+e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println("ERROR"+e.getMessage());

        }
    }


    public void insertObject(String msg) {
        splitObjects(msg);

    }

    public void splitObjects(String msg) {
    //copy values
        try {
            ToolRequest toolRequest = mapper.readValue(msg, ToolRequest.class);

            createRequest(toolRequest);


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void splitMessage(String msg) {
        String[] strings = msg.split("\n");


        for( String s: strings) {
            inserSensor(s);
        }
    }

    public void splitMessageTool(String msg) {
        String[] strings = msg.split("\n");


        for( String s: strings) {
            insertObject(s);
        }
    }


    public void inserSensor(String msg) {
        try {
            ToolRequest toolRequest = mapper.readValue(msg, ToolRequest.class);

            Request request = new Request();

            request.setProductionDate(toolRequest.getProductionDate());
            request.setToolId(toolRequest.getToolId());
            request.setSensorId(toolRequest.getToolId());


            if (request.getSensorId().equalsIgnoreCase("1")) {
                request.setPressure( toolRequest.getPressure().intValue());
            }

            if (request.getSensorId().equalsIgnoreCase("2")) {
                request.setTemp(toolRequest.getTemperature());
            }

            if (request.getSensorId().equalsIgnoreCase("3")) {
                request.setHumidity(toolRequest.getHumidity().intValue());
            }
            createRequestSensor(request);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
