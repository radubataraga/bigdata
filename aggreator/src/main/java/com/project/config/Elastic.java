package com.project.config;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Elastic extends AbstractFactoryBean {

//    @Value("${elastic.cluster}")
//    private String clusterName;
//    @Value("${elastic.host}")
//    private String address;
//    @Value("${elastic.port}")
//    private Integer port;


    private RestHighLevelClient restHighLevelClient;

    public Class<?> getObjectType() {
        return RestHighLevelClient.class;
    }

    protected Object createInstance() throws Exception {
//        return null;

        try {
            AuthScope scope = new AuthScope("localhost", 9200);
            final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(scope, new UsernamePasswordCredentials("elastic","changeme"));
            RestClientBuilder clientBuilder = RestClient.builder(new HttpHost("localhost", 9200))
                    .setHttpClientConfigCallback(
                            httpClientBuilder -> httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider));

            restHighLevelClient = new RestHighLevelClient(clientBuilder);
        }
        catch (Exception e) {
            System.out.println("########### ERROR");
        }


        return  restHighLevelClient;

    }
}
