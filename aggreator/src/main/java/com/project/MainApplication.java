package com.project;

import com.project.services.Aggregator;
import org.apache.commons.io.LineIterator;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.hadoop.fs.FsShell;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@SpringBootApplication
public class MainApplication implements CommandLineRunner {

    @Autowired
    private FsShell shell;

    @Autowired
    private Aggregator aggregator;

    public void run(String... args) {
    try {
        String path1 = "/home/andrei/Documents/Babes/An2/Sem1/BigData/bigdata/aggreator/src/main/resources/file1.txt";
        String path2 = "/home/andrei/Documents/Babes/An2/Sem1/BigData/bigdata/aggreator/src/main/resources/file2.txt";
//
        shell.copyFromLocal(path1, "/tmp/tool3.txt");
        shell.copyFromLocal(path2, "/tmp/sensors3.txt");


        System.out.println(shell.ls(true, "/"));
//
//        for (FileStatus s : shell.lsr("/tmp")) {
//            System.out.println("> " + s.getPath());
//        }
        readFile("/tmp/tool3.txt");
        readSecFile("/tmp/sensors3.txt");
//        System.out.println(shell.text("/tmp/file3.txt"));
//        System.out.println(shell.text("/tmp/file4.txt"));
        } catch (Exception e) {
        e.printStackTrace();
    }

    }

    public static void main(String[] args) {
        SpringApplication.run(com.project.MainApplication.class, args);
    }


    private void readFile(String path) {
        try {
            shell.text(path).forEach(x -> aggregator.splitMessageTool(x));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void readSecFile(String path) {
        try {
            shell.text(path).forEach(x -> aggregator.splitMessage(x));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}